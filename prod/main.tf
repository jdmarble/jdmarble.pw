terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/22678596/terraform/state/prod"
    lock_address = "https://gitlab.com/api/v4/projects/22678596/terraform/state/prod/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/22678596/terraform/state/prod/lock"
    lock_method = "POST"
    unlock_method = "DELETE"
    retry_wait_min = 5
  }
  required_providers {
    local = {
      source = "hashicorp/local"
      version = "2.0.0"
    }
  }
}

provider "kubernetes" {
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

module "argocd_bootstrap" {
  source = "../modules/argocd-bootstrap"
  target_revision = var.target_revision
  argocd_admin_password = var.argocd_admin_password
  b2_longhorn_bucket_application_key = var.b2_longhorn_bucket_application_key
  b2_longhorn_bucket_key_id = var.b2_longhorn_bucket_key_id
  dontstarvetogether_token = var.dontstarvetogether_token
  dontstarvetogether_password = var.dontstarvetogether_password
  foundryvtt_adminkey = var.foundryvtt_adminkey
  foundryvtt_username = var.foundryvtt_username
  foundryvtt_password = var.foundryvtt_password
}
