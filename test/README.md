If you use Bitwarden for secrets management, you can extract the necessary secrets as variables using `bw` and `jq`.
This command assumes the item with the given ID has custom fields for each secret documented in `variables.tf`.

```shell
$ export BW_SESSION=$(bw unlock --raw)
$ source <(bw get item "test.jdmarble.pw" | jq $'.fields[] | ("export TF_VAR_"+.name+"=\'"+.value+"\'")' --raw-output)
```

Before the first run, you must let Terraform scan the code and download the required dependencies.
This also configures the authentication for the remote state.

```shell
$ export TF_HTTP_USERNAME=$(bw get username 'gitlab.com')
$ export TF_HTTP_PASSWORD=$(bw get item 'gitlab.com' | jq '.fields[] | select(.name=="test.jdmarble.pw").value' --raw-output)
$ terraform init
```

If you are working on a branch, you probably want ArgoCD to pull from the same branch.

```shell
$ export TF_VAR_target_revision=$(git rev-parse --abbrev-ref HEAD)
```

Now apply!

```shell
$ terraform apply
```
