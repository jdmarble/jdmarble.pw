terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/22678596/terraform/state/test"
    lock_address = "https://gitlab.com/api/v4/projects/22678596/terraform/state/test/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/22678596/terraform/state/test/lock"
    lock_method = "POST"
    unlock_method = "DELETE"
    retry_wait_min = 5
  }
  required_providers {
    local = {
      source = "hashicorp/local"
      version = "2.0.0"
    }
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "1.22.2"
    }
  }
}

provider "digitalocean" {
  token = var.digitalocean_token
}

resource "digitalocean_kubernetes_cluster" "test_jdmarble_pw_cluster" {
  name = "test-jdmarble-pw-cluster"
  region = "sfo3"
  version = "1.19.3-do.2"

  node_pool {
    name = "worker-pool"
    size = "s-4vcpu-8gb"
    node_count = 3
  }
}

resource "local_file" "digitalocean_kubeconfig" {
  sensitive_content = digitalocean_kubernetes_cluster.test_jdmarble_pw_cluster.kube_config[0].raw_config
  filename = "kubeconfig.yaml"
  file_permission = "0600"
}

provider "helm" {
  kubernetes {
    host = digitalocean_kubernetes_cluster.test_jdmarble_pw_cluster.endpoint
    token = digitalocean_kubernetes_cluster.test_jdmarble_pw_cluster.kube_config[0].token
    cluster_ca_certificate = base64decode(
      digitalocean_kubernetes_cluster.test_jdmarble_pw_cluster.kube_config[0].cluster_ca_certificate
    )
  }
}

provider "kubernetes" {
  host = digitalocean_kubernetes_cluster.test_jdmarble_pw_cluster.endpoint
  token = digitalocean_kubernetes_cluster.test_jdmarble_pw_cluster.kube_config[0].token
  cluster_ca_certificate = base64decode(
    digitalocean_kubernetes_cluster.test_jdmarble_pw_cluster.kube_config[0].cluster_ca_certificate
  )
}

module "argocd_bootstrap" {
  source = "../modules/argocd-bootstrap"
  target_revision = var.target_revision
  argocd_admin_password = var.argocd_admin_password
  b2_longhorn_bucket_application_key = var.b2_longhorn_bucket_application_key
  b2_longhorn_bucket_key_id = var.b2_longhorn_bucket_key_id
  dontstarvetogether_token = var.dontstarvetogether_token
  dontstarvetogether_password = var.dontstarvetogether_password
  foundryvtt_adminkey = var.foundryvtt_adminkey
  foundryvtt_username = var.foundryvtt_username
  foundryvtt_password = var.foundryvtt_password
}
