variable "target_revision" {
  description = "The branch or other reference used to tell ArgoCD which part of the repository to deploy"
  type = string
}

variable "argocd_admin_password" {
  description = "Used to access the Argo CD user interface."
  type = string
  sensitive = true
}

variable "b2_longhorn_bucket_key_id" {
  description = "The ID of the key used to access Backblaze bucket for Longhorn backups."
  type = string
}

variable "b2_longhorn_bucket_application_key" {
  description = "The secret API key to access the Backblaze bucket for Longhorn backups."
  type = string
  sensitive = true
}

variable "dontstarvetogether_token" {
  description = "Needed to run a public DST dedicated server. Get one at https://accounts.klei.com/account/game/servers?game=DontStarveTogether"
  type = string
  sensitive = true
}

variable "dontstarvetogether_password" {
  description = "The password used by the DST player to join the server."
  type = string
  sensitive = true
}

variable "foundryvtt_adminkey" {
  description = "Used to administrate the Foundry VTT instance."
  type = string
  sensitive = true
}

variable "foundryvtt_username" {
  description = "The name of the user authorized to download Foundry VTT software."
  type = string
}

variable "foundryvtt_password" {
  description = "The password of the user authorized to download Foundry VTT software."
  type = string
  sensitive = true
}
