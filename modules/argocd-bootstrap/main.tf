terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "1.13.3"
    }
    helm = {
      source = "hashicorp/helm"
      version = "2.0.1"
    }
  }
}

resource "kubernetes_namespace" "argocd" {
  metadata {
    name = "argocd"
  }
}

resource "kubernetes_secret" "argocd" {
  depends_on = [kubernetes_namespace.argocd]
  metadata {
    name = "argocd-secret"
    namespace = "argocd"
    labels = {
      "app.kubernetes.io/name" = "argocd-secret"
      "app.kubernetes.io/managed-by" = "Helm"
      "app.kubernetes.io/part-of" = "argocd"
    }
    annotations = {
      "meta.helm.sh/release-name" = "argocd"
      "meta.helm.sh/release-namespace" = "argocd"
    }
  }
  type = "Opaque"
  data = {
    "admin.password" = var.argocd_admin_password
  }
}

resource "helm_release" "argocd_helm_release" {
  name = "argocd"
  chart = "../apps/argocd"
  dependency_update = true
  namespace = "argocd"
  values = [
    "{'spec': {'source': {'targetRevision': '${var.target_revision}'}}}"
  ]
}

resource "kubernetes_namespace" "dontstarvetogether" {
  metadata {
    name = "dontstarvetogether"
  }
}

resource "kubernetes_secret" "dontstarvetogether" {
  depends_on = [kubernetes_namespace.dontstarvetogether]
  metadata {
    name = "dontstarvetogether"
    namespace = "dontstarvetogether"
  }
  type = "Opaque"
  data = {
    TOKEN = var.dontstarvetogether_token
    PASSWORD = var.dontstarvetogether_password
  }
}

resource "kubernetes_namespace" "longhorn-system" {
  metadata {
    name = "longhorn-system"
  }
}

resource "kubernetes_secret" "backblaze-secret" {
  depends_on = [kubernetes_namespace.longhorn-system]
  metadata {
    name = "backblaze-secret"
    namespace = "longhorn-system"
  }
  type = "Opaque"
  data = {
    AWS_ACCESS_KEY_ID = var.b2_longhorn_bucket_key_id
    AWS_SECRET_ACCESS_KEY = var.b2_longhorn_bucket_application_key
    AWS_ENDPOINTS = "https://s3.us-west-000.backblazeb2.com"
  }
}

resource "kubernetes_namespace" "foundryvtt" {
  metadata {
    name = "foundryvtt"
  }
}

resource "kubernetes_secret" "foundryvtt" {
  depends_on = [kubernetes_namespace.foundryvtt]
  metadata {
    name = "foundryvtt"
    namespace = "foundryvtt"
  }
  type = "Opaque"
  data = {
    adminKey = var.foundryvtt_adminkey
    username = var.foundryvtt_username
    password = var.foundryvtt_password
  }
}
